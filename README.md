# PCY Park Chen Yu Algorithm for Finding Frequent Itemsets

Implemented in Python.

My lab assignment in Analysis of Massive Datasets, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2021