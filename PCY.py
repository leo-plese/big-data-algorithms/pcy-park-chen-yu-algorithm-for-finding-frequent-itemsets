from sys import stdin
from math import floor
from itertools import combinations


def pcy(br_kosara, br_pretinaca, prag, kosare):
    br_predmeta = {}

    for kosara in kosare:
        for i in kosara:
            br_predmeta[i] = br_predmeta.get(i, 0) + 1

    br_razlicitih_predmeta = len(br_predmeta)

    ij_h_map = {}
    # 2. prolaz
    pretinci = {}  # map:  rbr pretinca -> freq pretinca

    for i in range(br_kosara):
        ij_pairs = combinations(kosare[i], 2)

        for predmet_i, predmet_j in ij_pairs:
            if (br_predmeta[predmet_i] >= prag and br_predmeta[predmet_j] >= prag):
                hsh = ij_h_map.get((predmet_i, predmet_j))
                if hsh:
                    k = hsh
                else:
                    k = (predmet_i * br_razlicitih_predmeta + predmet_j) % br_pretinaca
                    ij_h_map[predmet_i, predmet_j] = k

                pretinci[k] = pretinci.get(k, 0) + 1

    # 3. prolaz
    parovi = {}  # map:   par (i,j) -> freq para

    for i in range(br_kosara):

        ij_pairs = combinations(kosare[i], 2)

        for predmet_i, predmet_j in ij_pairs:
            if (br_predmeta[predmet_i] >= prag and br_predmeta[predmet_j] >= prag):
                if pretinci[ij_h_map[predmet_i, predmet_j]] >= prag:
                    parovi[(predmet_i, predmet_j)] = parovi.get((predmet_i, predmet_j), 0) + 1

    # A
    m = sum([br_predmeta[i] >= prag for i in br_predmeta])
    print(m * (m - 1) // 2)
    # P
    print(len(parovi))

    X_list = list(parovi.values())
    X_list = [f for f in X_list if f >= prag]
    X_list.sort(reverse=True)

    for xi in X_list:
        print(xi)


if __name__ == "__main__":
    lines = stdin.readlines()

    br_kosara = int(lines[0].strip())
    s = float(lines[1].strip())
    br_pretinaca = int(lines[2].strip())

    prag = floor(s * br_kosara)

    kosare = [list(map(int, lines[i].split())) for i in range(3, 3 + br_kosara)]

    pcy(br_kosara, br_pretinaca, prag, kosare)